import { Component, OnInit } from '@angular/core';
import {InvoicesService} from './invoices.service';

@Component({
  selector: 'jce-invoices',
  templateUrl: './invoices.component.html',
  styles: [`
    .invoices li { cursor: default; }
    .invoices li:hover { background: #ecf0f1; }
    .list-group-item.active, 
    .list-group-item.active:hover { 
         background-color: #ecf0f1;
         border-color: #ecf0f1; 
         color: #2c3e50;
    }     
  `]
})
export class InvoicesComponent implements OnInit {

 invoices;

 currentInvoice;

 isLoading = true;

  constructor(private _invoicesService: InvoicesService) {
    //this.invoices = this._invoiceService.getInvoices();
  }
 select(invoice){
		this.currentInvoice = invoice; 
    //console.log(	this.currentInvoice);
 }
  
  deleteInvoice(invoice){
      this._invoicesService.deleteInvoice(invoice);
  }

  addInvoice(invoice){
    this._invoicesService.addInvoice(invoice);
  }

  editInvoice(invoice){
     this._invoicesService.updateInvoice(invoice); 
  }  
  


  ngOnInit() {
        this._invoicesService.getInvoices()
			    .subscribe(invoices => {this.invoices = invoices;
                               this.isLoading = false;
                               console.log(invoices)});
  }

}