import { Injectable } from '@angular/core';
import { Http }       from '@angular/http';
import {AngularFire} from 'angularfire2'; 
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/map';

@Injectable()
export class InvoicesService {
  private _url = "http://jsonplaceholder.typicode.com/invoices";
  invoicesObservable;

  constructor(private af:AngularFire) { }
  
  addInvoice(invoice){
    this.invoicesObservable.push(invoice);
  }

  deleteInvoice(invoice){
    this.af.database.object('/invoices/' + invoice.$key).remove();
    console.log('/invoices/' + invoice.$key);
  }

  updateInvoice(invoice){
    let invoice1 = {name:invoice.name,amount:invoice.amount}
    console.log(invoice1);
    this.af.database.object('/invoices/' + invoice.$key).update(invoice1)
  }  

  getInvoices(){
    this.invoicesObservable = this.af.database.list('/invoices').map(
      invoices =>{
        invoices.map(
          invoice => {
            invoice.posTitles = [];
            for(var p in invoice.posts){
                invoice.posTitles.push(
                this.af.database.object('/posts/' + p)
              )
            }
          }
        );
        return invoices;
      }
    )
    //this.invoicesObservable = this.af.database.list('/invoices');
    return this.invoicesObservable;
	}
}